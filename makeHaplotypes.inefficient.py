import pandas as pd
ancestries = ('African', 'European', 'Amerindian')
numbers = range(1, 23)
chromosomes = [number for number in numbers]

minSNPs = 7
maxSNPs = 20
maxRate = 0.3
for ancestry in ancestries:
 haplotype = 1
 haplotypes = []
 for chromosome in chromosomes:
  famFile = 'masked/masked'+ ancestry+ 'ChrAllAboveMinimum.fam'
  famData = pd.read_table(famFile,index_col=1,header=None)
  mapFile = 'masked/rfMixInChr'+ str(chromosome)+'_chr'+str(chromosome)+'.map'
  mapData = pd.read_table(mapFile,header=None)
  leftSNP = 0
  rightSNP = 0
  snps =  []
  nrows=len(mapData.index)
  numbers = range(0,nrows)
  rowindices = [number for number in numbers]
  for i in rowindices:
   if rightSNP > (nrows-2):
     break
   rightSNP = rightSNP + 1
   if mapData.iat[rightSNP, 1] - mapData.iat[leftSNP, 1] >= maxRate:
    if rightSNP - leftSNP >= minSNPs:
     snps = mapData.iloc[leftSNP:(rightSNP - 1), 2].tolist()
    else:
     leftSNP = rightSNP
     rightSNP = leftSNP + 1

              # If the haplotype is long enough
   if rightSNP - leftSNP >= maxSNPs:
    snps = mapData.iloc[leftSNP: (rightSNP - 2), 2].tolist()

  # If SNPs isn't NULL, then find the save the range of the window
   if (snps):
    haplotype = haplotype + 1
    mylines=[chromosome,leftSNP,rightSNP,mapData.iat[rightSNP - 2,1] - mapData.iat[leftSNP,1]]
    haplotypes.append(mylines)
    snps=[]
    leftSNP = rightSNP

pd.DataFrame(haplotypes).to_csv('africanHaplotypes2.tsv',sep='	', header=False, index=False)

